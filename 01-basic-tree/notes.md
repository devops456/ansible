# Les variables

Commandes a lancer pour afficher la variable declaree dans le fichier inventory.yml
	`ansible -i 00_inventory.yml all -m debug -a 'msg={{var1}}'`
Mauvaise pratique !! -> Les variables doivent etre definies dans des fichiers dedies (exple : group_vars/nodes.yml ou hosts_vars/srv.dev.yml)

## Group_vars

Sur la gestion des variables:
- Soit par fichier:
```
group_vars/
├── locals.yml
├── nodes.yml
└── remotes.yml
```

- Soit par repertoire
```
group_vars/
├── locals
│   └── variables.yml # Le nom du fichier est a notre main
├── nodes
│   └── variables.yml
└── remotes
    └── variables.yml
```
Preferez cette structure pour la gestion des vaulted variables

## Host_vars

Comme precedemment, deux gestions possibles:
* par fichier
* par repertoire (le choix ici)

Ces variables surchargent les varibles de group

Extrait de la commande ansible:
```bash
 $ ansible -i 00_inventory.yml all -m debug -a 'msg={{var1}}'
node-local-1 | SUCCESS => {
    "msg": "local node : variable from host var"
}
node-remote-1 | SUCCESS => {
    "msg": "remote node value from group_vars"
}
node-remote-2 | SUCCESS => {
    "msg": "remote node value from group_vars"
}
```

## Afficher les valeurs des variables

Pour afficher les valeurs que prendront les variables (dans le cadre d'un debug par exple) on pourra utiliser la commande suivante: `ansible-inventory -i 00_inventory.yml --list --yaml` (le `--yaml` etant facultatif format json par defaut).
<br/>Dans une version un peu plus visuelle : `$ ansible-inventory -i 00_inventory.yml --graph --vars` (le `--var`s est facultatif).

```bash
$ ansible-inventory -i 00_inventory.yml --graph --vars
@all:
  |--@common:
  |  |--@nodes:
  |  |  |--@locals:
  |  |  |  |--node-local-1
  |  |  |  |  |--{var1 = local node : variable from host var}
  |  |  |  |--{var1 = local node : value from group vars}
  |  |  |--@remotes:
  |  |  |  |--node-remote-1
  |  |  |  |  |--{var1 = remote node value from group_vars}
  |  |  |  |--node-remote-2
  |  |  |  |  |--{var1 = remote node value from group_vars}
  |  |  |  |--{var1 = remote node value from group_vars}
  |  |  |--{var1 = value from group vars}
  |--@ungrouped:
  ```