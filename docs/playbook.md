# Les playbooks

__playbook:__
* fichier déclenchant les actions à réaliser : les _tasks_
* sert à articuler l'inventory avec les roles
* peut inclure des tasks: Mauvaise pratique !
* peut inclure des variables: Mauvaise pratique !
* peut faire tout ce que fait un rôle (globalement)
* spécifier un user

La commande : `$ ansible-playbook`
<br/>nombreuses options:
* `-i` : inventaire
* `-l` : limit > spécifier un/des groupes ou serveurs ou patterns
* `-u` : user
* `-b` : become > sudo
* `-k` : password de ssh (Mauvaise pratique)
* `-K` : password de sudo
* `-C` : check > dry run
* `-D` : diff > afficher les différences avant/apres les tasks
* `--ask-vault` : prompt pour le password vault
* `--vault-password-file` : passer le vault password par un fichier
* `-e` : surcharge n'importe quelle variable (prioritaire)
* `-f` : nombre de parallelisation (default = 5)
* `-t` : filtrer sur les tags (--skip-tags). Les roles/tasks peuvent être taggés, on peut donc jouer un playbook en spécifiant les tags
* `--flush-cache` : évitier l'utilisation du cache
* `--step` : une tâche a la fois (confirmation via prompt > break point)
* `--start-at-task` : commencer à une tache en particulier
* `--list-tags` : liste tous les tags rencontrés
* `--list-tasks` : liste toutes les tâches qui vont être rencontrée
