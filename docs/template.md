# Module template

Objectif: generation de fichier a partir de modeles integrant des variables (format jinja)

## Parametres

* `attributes`: atrributs de fichier
* `backup`: creer une savegarde avant la modification (horodate)
* `block_start_string`, et `block_end_string`: delimitateur `{%` `%}`
* `dest`: fichier cible ou genere
* `follow`: suivre les liens symboliques
* `force`: ecrase le fichier (default yes)
* `group`: group proprietaire du fichier
* `lstrip_blocks`: respect stricte ou non des tabulations et blancs. Pour l'activier dans le fichier de template : `#jinja2:lstrip_blocks: True` en debut de fichier
* `mode`: permission
* `newline_sequence`: quel element est utilise pour les nouvelles lignes 
* `output_encoding`: encodage du fichier genere ( default = UTF8)
* `owner`: propietaire
* `src`: fichier source (template), attention localisation
  * attention dans le cas d'utilisation de role: `role/dir/template/`
* `trim_blocks`: supprimer les retours a la lignes des bocks
* `unsafe_writes`: eviter la corruption des fichiers
* `validate`: commande de validation avant les modifications
* `variable_start_string` et `variable_end_string`: caractere Jinja : `{{` et `}}`

## Variables ansible dediees aux templates

* `ansible_managed`: pour afficher en début de chaque template (prevenir)
* `template_host`: machine qui a joue le template
* `template_uid`: user a l'origine de la modification via le template
* `template_path`: localisation du fichier
* `template_fullpath`: chemin complet
* `template_run_date`: date de modification

Exemple en début de fichier: `#{{ template_run_date }} - "{{ ansible_managed }}" via {{ template_uid }}@{{ template_host }}`
