# Conditions et boucles

## Condition

Pour conditionner l'éxecution d'une tâche il faut utiliser le mot clef : `when`
<br/>Exemple :

```yaml
- name: Creation d'un repertoire
  file:
    path: /tmp/my_dir
    state: directory
  when: __output_stat_file.stat.exists == true
```

Ici la création du répertoire `my_dir` est conditionée par la valeur de la variable `__output_stat_file.stat.exists`
<br/>Cette variable est elle même contenue dans le dictionnaire `__output_stat_file` (2 niveaux : stat et exists) qui a été définie dans une task avant grâce au mot clef `register`

```yaml
- name: test de stat
  stat:
    path: /tmp/file.txt
  register: __output_stat_file
```

Ce dictionnaire python peut être parcouru, afin de cibler une valeur spécifique, grâce a la tâche suivante:

```yaml
- name: affichage de l'output
  debug:
    var: __output_stat_file
```

## Les boucles
[Documentation generique](https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html)<br/>
[Documentation sur le with_item](https://docs.ansible.com/ansible/latest/plugins/lookup/items.html)

Les boucles sont génerée à partir du mot clef `with_*` : en effet il existe plusieurs types de boucles:

* `with_items`: liste de dictionnaires
* `with_nested`: liste de listes
* `with_dict`: parcours d'un dictionnaire
* `with_fileglob`: liste des fichiers avec un pattern non recursif
* `with_filetree`: liste des fichiers dans une arborescence (pattern) suivant des critères
* `with_together`: croisement des deux listes en parallèle
* `with_sequence`: avec des ranges à intervalle
* `with_random_choice`: tirage au sort dans une liste
* `with_first_found`: premier élément d'une liste (qui elle même est variable)
* `with_lines`: avec chaque ligne d'un programme (shell par exemple)
* `with_ini` : parcourir un fichier ini
* `with_inventory_hostnames` : avec l'inventory

Exemple dans [ce playbook](./playbook-loop.yaml):

La première tâche _create dir_, boucle sur les variables contenues dans `items` : les trois sous-dossiers

Vérification sur l'une des machines hôtes:
```bash
$ tree /tmp/
/tmp/
`-- my-dir
    |-- sub-dir1
    |-- sub-dir2
    `-- sub-dir3
```

La deuxième tâche _create dir and file_, quant à elle, boucle sur les dictionnaires contenus dans `items`. Dans ce cas, cela nous permet de spécifier un fichier associé à chaque repertoire.

Vérification sur l'une des machines cliente:
```bash
$ tree /tmp
/tmp
`-- another-dir
    |-- sub-dir1
    |   `-- file1.1.txt
    |-- sub-dir2
    |   `-- file2.1.txt
    `-- sub-dir3
        |-- file3.1.txt
        `-- file3.2.txt
```
