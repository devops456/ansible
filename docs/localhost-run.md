# Local run

L'idée ici est de pouvoir lancer des tâches en local. Ansible nous propose plusieurs solutions.<br/>
Cela peut s'avérer utile dans le cadre d'une configuration d'une VM nouvellement créer

## `host: localhost`

La première solution consiste à run l'ensemble des tâches sur en spécifiant `localhost` dans le fichier d'inventory.<br/>
L'inconvénient majeur de cette solution est qu'il faut au préalable faire un échange de clef ssh car ansible se connecte a ses nodes via une connexion ssh.

## `local_action`

Permet de passer une commande en local, mais toujours en utilisant ssh

```yaml
  - name: local action
    local_action: "command touch /tmp/myfile.txt"
```

Cette tâche créera un fichier `my_file.txt`, autant qu' il y aura d'host déclarer.<br/>
La connexion sera également en ssh.

## `connection: local`

Considérons le playbook:
```yaml
- name: connection local
  hosts: localhost    
  connectio: local
  tasks:
  - name: local action
    file:
      state: touch
      path: /tmp/myfile.txt
```

Il créera le fichier demande à l'endroit indiqué mais **sans recours à la connecion ssh**.<br/>
Cette fonctionnalité peut s'avérer très pratique dans le cas de la configuration d'une nouvelle VM.

## `delegate_to`

Permet d'éxecuter la tâche sur un serveur spécifique :

```yaml
- name: local run 
  hosts: containers
  become: yes 
  tasks:
  - name: creation d'un fichier en local
    file:
      state: touch
      path: /tmp/myfile.txt
    delegate_to: localhost
```

Ici le création du fichier *myfile.txt* se fera uniquement sur le localhost, là encore avec une connexion ssh. La tâche se lancera autant de fois qu'il y a de hosts. Ici mon group `containers` contient 3 hosts, il y aura donc 3 créations de fichier.

Pour éviter cela on utilise la commande `run_once: yes`

```yaml
  - name: creation d'un fichier en local
    file:
      state: touch
      path: /tmp/myfile.txt
    delegate_to: localhost
    run_once: yes
```

### Cas des variables

Considérons le fichier d'inventaire suivant:
```yaml
all:
  vars:
    ansible_python_interpreter: /usr/bin/python3
  children:
    172.17.0.2:
      my_var: VALUE 1
    172.17.0.3:
      my_var: VALUE 2
    172.17.0.4:
      my_var: VALUE 3
```

Et le playbook suivant:

```yaml
- name: playbook de test
  hosts: all
  become: yes
  tasks:
  - name: debug sans delegate
    debug:
      var: my_var

  - name: debug avec delegate
    debug:
      var: my_var
    delegate_to: 172.17.0.2
```

Ce playbook, affichera les valeurs des variables `my_var`, pour deux tâches:
- sans le `delegate_to`
- avec le `delegate_to`

On s'attend à ce que la valeur de la variable soit differente pour la première tâche et différente pour la seconde:
```bash
TASK [debug sans delegate] ************************************************************************************************************
Friday 12 February 2021  11:30:52 +0100 (0:00:00.830)       0:00:00.851 ******* 
ok: [172.17.0.2] => {
    "my_var": "VALUE 1"
}
ok: [172.17.0.3] => {
    "my_var": "VALUE 2"
}
ok: [172.17.0.4] => {
    "my_var": "VALUE 3"
}

TASK [debug qvec delegate] ************************************************************************************************************
Friday 12 February 2021  11:30:52 +0100 (0:00:00.091)       0:00:00.943 ******* 
ok: [172.17.0.2 -> 172.17.0.2] => {
    "my_var": "VALUE 1"
}
ok: [172.17.0.3 -> 172.17.0.2] => {
    "my_var": "VALUE 2"
}
ok: [172.17.0.4 -> 172.17.0.2] => {
    "my_var": "VALUE 3"
}
```

**On remarque que même pour la deuxième tâche la valeur est specifique à l'host et non à la machine specifiée dans le `delegate_to`**



