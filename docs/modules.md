# Les modules

__Documentation:__
* En ligne : https://docs.ansible.com/ansible/2.9/modules/list_of_all_modules.html
* En local (exemple avec le module file) : `$ ansible-doc file`

## Un exemple de module utilsé : `file`

**Les parametres:** (liste non exhaustive)
* attribute : paramêtres particuliers d'un fichier : immutabilité (stickybit)
* force : pour les lien symboliques (si le fichier source n'existe pas créé quand meme le ligne)
* group/owmer : équivalent `$ chown`
* mode : équivalent `$ chmod` : "0755" ou "u=rwx,g=rx,o=rx"
* path : chemin de fichier
* recurse : crée l'arborescence équivalent `$ mkdir -p`
* src : pour les liens symboliques (soft, hard)
* state:
  * absent = suppression
  * present = creation
    * directory
    * file : vérifie la présence du fichier
    * hard (hard link)
    * link (soft link)
    * touch : crée le fichier

## Utilisation dans un playbook

* [lien vers le playbook](playbook.yml)

```bash
$ ansible-playbook -i 00_inventory.yml playbook.yml 
PLAY [mon playbook] *******************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************************
Tuesday 19 January 2021  18:16:40 +0100 (0:00:00.013)       0:00:00.013 ******* 
ok: [node-local-1]

TASK [check connection] ***************************************************************************************************************
Tuesday 19 January 2021  18:16:41 +0100 (0:00:00.859)       0:00:00.873 ******* 
ok: [node-local-1]

TASK [creation d'un repertoire /tmp/dir-test] *****************************************************************************************
Tuesday 19 January 2021  18:16:41 +0100 (0:00:00.266)       0:00:01.139 ******* 
changed: [node-local-1]

PLAY RECAP ****************************************************************************************************************************
node-local-1               : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

Tuesday 19 January 2021  18:16:42 +0100 (0:00:00.298)       0:00:01.438 ******* 
=============================================================================== 
Gathering Facts ---------------------------------------------------------------------------------------------------------------- 0.86s
creation d'un repertoire /tmp/dir-test ----------------------------------------------------------------------------------------- 0.30s
check connection --------------------------------------------------------------------------------------------------------------- 0.27s
```
* Vérification:

```bash
$ ssh node-local-1 
Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 4.15.0-132-generic x86_64)
...

$ ansible@node-local-1:~$ ls -d /tmp/dir-test
/tmp/dir-test
```
