# Gestion de clef ssh

## `openssh_keypair` : module pour la generation de clef

Exemple de task permettant la generation de clefs ssh:

```yaml
  - name: generation de la clef
    openssh_keypair:
      path: /tmp/ssh-ansible
      regenerate: full_idempotence
      type: rsa
      size: 4096
      state: present
      force: no
    delegate_to: localhost
    run_once: yes # Pour lancer la generation qu'une seule fois
```

* `path`: chemins sera cree le couple de clef, ici `ssh-ansible` et `ssh-ansible.pub`
* `regenerate`:
  * `never` : On la cree une fois mais on le regenere jamais
  * `fail` : Regenere si la clef est HS
  * `partial_idempotence`: Si la clef est non conforme a la description Ansible la regenere
  * `full_idempotence` : si elle est a la fois non conforme ou illisble (HS)
  * `always` : On recree une clef systematiquement
* `type`, `size`: proprietes de la clef
* `state`: si `present` creation de la clef
* `force`: si `yes` regenere la clef meme si elle existe deja
* `delegate_to: localhost` permet de jouer la task sur le localhost. Ici cree le couple de clef en local
* `run_once: yes`: Lance le job une seule, sinon le job sera relancer autant de fois qu'il y a de node declares dans l'inventaire.

## `authorized_key` : module pour le deploiement de la clef

```yaml
  - name: Deploy SSH Key
    become: yes
    authorized_key:
      user: devops
      key: "{{ lookup('file', '/tmp/ssh-ansible.pub') }}"
      state: present
```

* `become: yes`: execute les tache en tant que root
* `user: devops`: de quel user il s'agit. Ici *devops* qui a ete prealabelement cree [cf playbook task **creation du user deops**](./playbook-ssh-key.yml).
* `key`: prend la clef en tant que string, grace a la valuer suivante : `"{{ lookup('file', '/tmp/ssh-ansible.pub') }}"`, cela evite d'avoir a reecrire la clef
* `state: presente`: depose la clef
