# Inventory et variables

- L'inventory est composé de :
  - inventory.ini/yaml/json
  - group_vars
  - hosts_vars

_n.b : Pour le format préféré le YAML_

- type d'instance:
  - hosts : directement la machine
  - group : un groupe de machine

## Les variables ansibles

Les variables ont un ordre hiérarchique

- Regroupement par famille:
  - Configuration settings
  - Command-line option
  - Playbook keywords
  - Variables

Il en existe 22 types

Lien vers la doc: https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html


## Exemple de fichier inventory (format yaml)

```yaml
all: # group racine
  children:
    parent1:
      hosts:
        srv4:
      children:
        enfant1:
          hosts:
            srv1:
            srv2:
        enfant2:
          hosts:
            srv3:
      children:
        enfant3:
          hosts:
            srv5:
```

ou

```yaml
all:
  children:
    web-servers:
      children:
        nginx-servers:
          hosts:
            my-nginx-1: # possibilite d'utiliser un pattern : my-nginx-[1:2]:
            my-nginx-2:
        apache-servers:
            my-apache-[1:2]:
    data-bases-servers:
      children:
        SQL-servers:
          hosts:
            my-sql-raid-1: # idem my-sql-raid-[1:3]:
            my-sql-raid-2:
            my-sql-raid-3:
        NoSQL-servers:
          hosts:
            mongo-[1:2]:

    monitoring:
      children:
        web-servers:
        data-bases-servers:
```

Exemple d'inventaire que j'utilse en local:

```yaml
all:
  vars:
    ansible_python_interpreter: /usr/bin/python3
  children:
    containers:
      hosts:
        mchevrel-debian-[1:3]:
    VMs:
      children:
        locals:
          hosts:
            node-local-1:
        remotes:
          hosts:
            node-remote-[1:2]:
```

