# Infrastructure

## Master

* VM VirtualBox
* Ubuntu 18.04
* RAM: 8Go
* CPU: 4

## Nodes

1 noeud local:
* VM VirtualBox
* Ubuntu 18.04 (server edition)
* RAM: 1Go
* CPU: 1
* hostname: node-local-1

2 noeuds distant (autre machine hote)
* VM VirtualBox
* CentOS 8
* RAM: 1Go
* CPU: 1
* hostname:
  * node-local-1
  * node-local-2

1 à 5 noeuds conteneurs sur VM
* Docker: image Debian
* [Dockerfile](../04-docker/VM/Dockerfile)
* [script de déploiement](../04-docker/VM/deploy.sh)

1 à 5 noeuds conteneurs Raspberry (CPU ARM)
* Docker-compose: image CentOS
* [Dockerfile](../04-docker/raspberry/)
* [script de déploiement](../04-docker/raspberry/deploy-rasp)