# Docker image build role

## Purpose 

This role is used to:
* Create a docker image
* Create a tar file which contains the docker image
* Push the docker image into a specified registry

## Requirements

You might install python (pip) and Ansible dependencies, please refer to the __Requirement__ section of the [Ansible documentation](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_image_module.html#ansible-collections-community-docker-docker-image-module)
