# Les handlers

Les handlers sont des tasks qui seront déclenchées par une autre task.

## Exemples

Reload du service __nginx__ lors d'une modification de configuration:

```yml
hosts: containers-VM
become: yes
tasks:
- name: installation d'un vhost
  template:
    src: template_vhost.conf.j2
    dest: /etc/nginx/sites-available/vhost.conf
    owner: root
    group: root
    mode: 0644
  notify: reload_nginx

handlers:
  - name: reload_nginx
    systemd:
      name: nginx
      state: reloaded
```

A retenir:
* le lien entre la tâche et le handler se fait grâce au mot clef: `notify`
* Le handler porte le même nom que la valeur du notify
* Le handler est __systematiquement__ joue en fin de playbook

### Gerer le declenchement du handlers

Jouer le handler à un instant donne:

On peut pallier au fait que les handlers sont joués en fin de playbook, en les déclenchants avec la task suivante:

```yaml
  - name: flush handlers
    meta: flush_handlers
```

Mais __attention !__ ce la jouera tous les handlers declarés dans le playbook !



### Affiner le declenchement du trigger

À l'aide de la clef : `changed_when`, on peut affiner le déclenchement du handler:

Ici un exemple avec un cas simple de création de fichier en tant que handler:

```yml

  - name: Action sur le trigger
    stat:
      path: /tmp/handlers-trigger.txt
    register: __delete_file
    changed_when: __delete_file.stat.exists
    notify: delete_file

  handlers:
    - name: delete_file
      file:
        path: /tmp/handlers-trigger.txt
        state: absent
```
