# Les roles

## 1. Arborescence

Structure:
* tasks: les actions et le point d'entree
* default: les variables par defaut
* vars: les variables de role
* handlers: les declencheurs
* templates: les fichiers jinja
* files: les fichiers a copier ou fichiers statiques
* meta: pour partager sur galaxy et inclure les dependances
* test: elements de tests
* library: modules specifiques au role

Pour creer un role via la ligne de commande : `$ ansible-galaxy init MON_ROLE`

```bash
> $ ansible-galaxy init MON_ROLE
- Role MON_ROLE was created successfully
> $ tree
.
└── MON_ROLE
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── README.md
    ├── tasks
    │   └── main.yml
    ├── templates
    ├── tests
    │   ├── inventory
    │   └── test.yml
    └── vars
        └── main.yml
```

## 2. Ansible galaxy

Hub de partage des roles: https://galaxy.ansible.com

