# Ansible

## Objectif

L'objectif de ce projet est le parcours de la technologie __Ansible__ au travers de la série de vidéos YouTube qui nous est proposée par __Xavki__

## Lien utiles

* [La chaine youtube xavki](https://www.youtube.com/channel/UCs_AZuYXi6NA9tkdbhjItHQ)
* [La série dédiée à Ansible](https://www.youtube.com/watch?v=kzmvwc2q_z0&list=PLn6POgpklwWoCpLKOSw3mXCqbRocnhrh-)

## [Infrastructure](docs/infra.md)

## [Inventory et varibles Ansible](docs/inventory.md)

## [Les modules](docs/modules.md)

## [Le module ssh-keypair](docs/ssh-keypair.md)

## [Les playbooks](docs/playbook.md)

## [Conditions et boucles](docs/localhost-run.md)

## [Les templates](docs/template.md)
 
