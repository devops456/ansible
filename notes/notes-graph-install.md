# Pour l'afficher de notre arborescence sur une image

* Installation des dependances
  * `$ pip3 install ansible-inventory-grapher`
  * `$ sudo apt install graphviz graphicsmagick-imagemagick-compat`

* Lancement du graph : `ansible-inventory-grapher -i 00_inventory.yml all | dot -Tpng | display png:-`

