# Idempotence et stateless

* Idempotence: N'effectue les actions que si la situation est differente
  Exemple : creation d'un fichier
```yaml
- name: file creation
  file:
    path: "/tmp/file.txt"
    state: present
```
Si le fichier existe deja pendant le lancement de cette tache ansible ne fera aucune action.
<br/>Lorsqu'on lance un playbook, c'est la difference entre `ok` et `changed`

Un objectif serait d'avoir 0 changes lorsque l'on joue une deuxieme fois le playbook

* Stateless : pas de stockage de l'etat de la machine.
Difference principale avec terraform.

Si je cree une VM avec Terraform, il la stocke dans sa base de donnees `.state` et si on supprime cette VM : Terraform compare avec son state et recree la VM, la ou Ansible ne faire rien car il n'y a pas de notion d'etat